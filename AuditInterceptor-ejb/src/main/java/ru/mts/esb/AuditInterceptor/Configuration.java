package ru.mts.esb.AuditInterceptor;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class Configuration {
    private static final Logger logger = Logger.getLogger(Configuration.class.getSimpleName());
    private static volatile Configuration instance;

    private Map<String,String> printClassMap = new LinkedHashMap<>();
    private Map<String,String> excludeMethodMap = new LinkedHashMap<>();
    private Map<String,String> printAlwaysMap = new LinkedHashMap<>();
    private boolean showNotInvokedMethod = false;



    public static Configuration getInstance() {
        Configuration localInstance = instance;
        if (localInstance == null) {
            synchronized (Configuration.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Configuration();
                }
            }
        }
        return localInstance;
    }

    public Configuration() {
        init();
    }

    public Map<String, String> getPrintClassMap() {
        return printClassMap;
    }

    public Map<String, String> getExcludeMethodMap() {
        return excludeMethodMap;
    }

    public Map<String, String> getPrintAlwaysMap() {
        return printAlwaysMap;
    }

    public boolean isShowNotInvokedMethod() {
        return showNotInvokedMethod;
    }

    private void init() {
        String printClass = ResourceBundle.getBundle("audit_settings").getString("printClass");
        String printAlways =
                "java.lang.Integer;" +
                "java.lang.Long;" +
                "java.lang.Byte;" +
                "java.lang.Double;" +
                "java.lang.Float;" +
                "java.lang.Short;" +
                "java.lang.Number;" +
                "java.lang.String;" +
                "java.lang.Boolean;" +
                "java.math.BigDecimal;" +
                "java.math.BigInteger;";
        printAlways += ResourceBundle.getBundle("audit_settings").getString("printAlways");


        String excludeMethod =
                "getSuperclass;" +
                "getAnnotations;" +
                "toString;" +
                "isInterface;" +
                "isArray;" +
                "isPrimitive;" +
                "getComponentType;" +
                "getAnnotationType;" +
                "getAnnotations;" +
                "getCanonicalName;" +
                "getClassLoader;" +
                "getClassLoader0;" +
                "getClasses;" +
                "getConstantPool;" +
                "getConstructors;" +
                "getDeclaredAnnotations;" +
                "getDeclaredClasses;" +
                "getDeclaredClasses0;" +
                "getDeclaredConstructors;" +
                "getDeclaredFields;" +
                "getDeclaredMethods;" +
                "getDeclaringClass;" +
                "getDeclaringClass0;" +
                "getEnclosingClass;" +
                "getEnclosingConstructor;" +
                "getEnclosingMethod;" +
                "getEnclosingMethod0;" +
                "getEnclosingMethodInfo;" +
                "getEnumConstants;" +
                "getEnumConstantsShared;" +
                "getFactory;getFields;" +
                "getGenericInfo;" +
                "getGenericInterfaces;" +
                "getGenericSignature;" +
                "getGenericSuperclass;" +
                "getInterfaces;" +
                "getMethods;" +
                "getName0;" +
                "getPackage;" +
                "getProtectionDomain;" +
                "getProtectionDomain0;" +
                "getRawAnnotations;" +
                "getReflectionFactory;" +
                "getSigners;" +
                "getSimpleName;" +
                "getTypeParameters;" +
                "isAnnotation;" +
                "isAnonymousClass;" +
                "isEnum;" +
                "isLocalClass;" +
                "isLocalOrAnonymousClass;" +
                "isMemberClass;" +
                "enumConstantDirectory;" +
                "newInstance;" +
                "privateGetPublicMethods;" +
                "reflectionData;" +
                "wait;" +
                "notify;" +
                "notifyAll;" +
                "getClass;" +
                "hashCode;" +
                "byteValueExact;" +
                "shortValueExact;" +
                "isSynthetic;";
        excludeMethod += ResourceBundle.getBundle("audit_settings").getString("excludeMethod");

        String[] a = excludeMethod.split(";");
        for(String key : a){
            key = key.trim();
            logger.fine("excludeMethod key: " + key);
            if(!excludeMethodMap.containsKey(key)){
                excludeMethodMap.put(key,key);
            }
        }

        a = printClass.split(";");
        for(String key : a){
            key = key.trim();
            logger.fine("printClass key: " + key);
            if(!printClassMap.containsKey(key)){
                printClassMap.put(key,key);
            }
        }

        a = printAlways.split(";");
        for(String key : a){
            key = key.trim();
            logger.fine("printObject key: " + key);
            if(!printAlwaysMap.containsKey(key)){
                printAlwaysMap.put(key,key);
            }
        }
        if (ResourceBundle.getBundle("audit_settings").getString("showNotInvokedMethod").equals("1")){
            showNotInvokedMethod = true;
        }

    }
}
