package ru.mts.esb.AuditInterceptor;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC,
        use = SOAPBinding.Use.ENCODED)
public interface ServiceInterceptorI {

    public boolean test(String msg);

}
