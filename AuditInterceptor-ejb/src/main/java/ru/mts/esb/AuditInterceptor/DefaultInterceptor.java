package ru.mts.esb.AuditInterceptor;


import com.sun.grizzly.http.servlet.HttpServletResponseImpl;
import com.sun.xml.ws.api.message.Header;
import com.sun.xml.ws.api.message.HeaderList;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.Date;
import java.util.logging.Logger;


public class DefaultInterceptor {
    private static final Logger logger = Logger.getLogger(DefaultInterceptor.class.getSimpleName());

    @AroundInvoke
    public Object logMethodEntry(InvocationContext invocationContext) throws Exception {

        Date start = new Date();
        String out = "";

        Object returnValue = invocationContext.proceed();


        try {
            out += "\n----------------------------------------------------------------------------------\n";
            out += ">>>Audit " + invocationContext.getMethod().getDeclaringClass().getName()
                    + "." + invocationContext.getMethod().getName() + "\n";

            if (invocationContext.getContextData().keySet().size() > 0) {
                out += ">1.Context Info: " + "\n";
                for (String key : invocationContext.getContextData().keySet()) {
                    switch (key) {
                        case "com.sun.xml.ws.api.message.HeaderList":
                            try {
                                out += "    >com.sun.xml.ws.api.message.HeaderList:\n";
                                for (Header header : (HeaderList) invocationContext.getContextData().get(key)) {
                                    out += "        " + header + ": " + invocationContext.getContextData().get(key) + "\n";
                                }
                            } catch (Exception e) {
                                logger.severe("Error com.sun.xml.ws.api.message.HeaderList: " + e);
                            }
                            break;
                        case "javax.xml.ws.servlet.response":
                            try {
                                out += "    >javax.xml.ws.servlet.response:\n";
                                HttpServletResponseImpl responce = (HttpServletResponseImpl) invocationContext.getContextData().get(key);
//                                out += "        HeaderNames:" + ": " + responce.getHeaderNames().toString() + "\n";
                                out += "        Message:" + ": " + responce.getMessage() + "\n";

                            } catch (Exception e) {
                                logger.severe("javax.xml.ws.servlet.response: " + e);
                            }
                            break;
                        default:
                            out += "    " + key + ": " + invocationContext.getContextData().get(key) + "\n";
                            break;
                    }

                }
            } else {
                out += ">1.Context Info: not found context!" + "\n";
            }

            if (invocationContext.getParameters() != null) {
                out += ">2.Parameters: " + "\n";
                for (Object param : invocationContext.getParameters()) {
                    if (param != null && param.getClass() != null) {
                        out += "    " + param.getClass().getName() + ": " + param + "\n";
                    }
                }

            } else {
                out += "NO PARAMETERS!";

            }

            Date end = new Date();
            long runtime = end.getTime() - start.getTime();
            out += ">3.Runtime: " + runtime + "\n";

            out += ">4.Return: " + returnValue.getClass().getName() + "\n";
            PrintClass printClass = new PrintClass();
            out += printClass.analise(returnValue, "||");


            out += "----------------------------------------------------------------------------------";
            logger.info(out);
        } catch (Exception e) {
            logger.severe("Error audit: " + e);
            e.printStackTrace();
        }

        return returnValue;


    }


}
