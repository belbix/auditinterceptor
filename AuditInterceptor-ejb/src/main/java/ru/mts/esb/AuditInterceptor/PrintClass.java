package ru.mts.esb.AuditInterceptor;


import javax.ejb.EJB;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.logging.Logger;

public class PrintClass {
    private static final Logger logger = Logger.getLogger(PrintClass.class.getSimpleName());

    @EJB
    private Configuration configuration = Configuration.getInstance();

    public String analise(Object object, String prefix) {
        String out = "";

        //TODO создать вывод в стринг класса, взятого из настроек по рефлексии
        switch (object.getClass().getName()) {
//            case "numlex.basic.NPResponse":
//                break;
            default:
                try {
                    Method[] methods = object.getClass().getMethods();
                    for (Method method : methods) {

                        if (method.getGenericParameterTypes().length > 0) continue;

                        Class returnClass = method.getReturnType();
                        String returnClassName = returnClass.getName();

//                        String returnGenericType = method.getGenericReturnType().toString();

                        if (returnClassName != null
                                && !returnClassName.equals("")
                                && !configuration.getExcludeMethodMap().containsKey(method.getName())
                                ) {


                            if (configuration.getPrintAlwaysMap().containsKey(returnClassName)
                                    || returnClass.isPrimitive()
                                    || returnClass.isAssignableFrom(Integer.class)
                                    || returnClass.isAssignableFrom(Byte.class)
                                    || returnClass.isAssignableFrom(Double.class)
                                    || returnClass.isAssignableFrom(Short.class)
                                    || returnClass.isAssignableFrom(Float.class)
                                    || returnClass.isAssignableFrom(Long.class)
                                    || returnClass.isAssignableFrom(String.class)
                                    || returnClass.isAssignableFrom(BigInteger.class)
                                    || returnClass.isAssignableFrom(BigDecimal.class)
                                    ) {

                                method.setAccessible(true);
                                try {
                                    Object r = method.invoke(object);
                                    if (r != null) {
                                        out += prefix + method.getName() //+ ": " + returnClassName
                                                + " = " + r + "\n";
                                    }
                                } catch (InvocationTargetException e) {
                                    logger.severe("Error invoke method:" + method.getName());
                                    e.printStackTrace();
                                }

                            } else {

                                if (configuration.getPrintClassMap().containsKey(returnClassName)) {
                                    PrintClass pc = new PrintClass();
                                    out += prefix + ">" + method.getName() + "->" + returnClassName + "\n";
                                    try {
                                        out += pc.analise(method.invoke(object), prefix + "||");
                                    } catch (InvocationTargetException e) {
                                        logger.severe("Error invoke method:" + method.getName());
                                        e.printStackTrace();
                                    }
                                } else {
                                    if (configuration.isShowNotInvokedMethod()) {
                                        out += prefix + "-" + method.getName() + ": " + returnClassName
                                                + ": " + returnClass + "\n";
                                    }
                                }

                            }
                        }

                    }
                } catch (Exception e) {
                    logger.severe(prefix + ": Error " + object.getClass().getName() + ": " + e);
                    e.printStackTrace();
                }
                break;
        }


        return out;
    }

}
